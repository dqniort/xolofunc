'use strict';

const functions = require('firebase-functions');
const nodemailer = require('nodemailer');
const admin = require('firebase-admin');
admin.initializeApp();

// functions.config().gmail.email; //"xolopetapp@gmail.com";
// const gmailPassword =    //
//xxwczntgsysriofv
const mailTransport = nodemailer.createTransport({
    host: 'smtp.gmail.com', port: 465, secure: true,
    // service: 'Gmail',
    auth: {
        user: 'xolopetapp@gmail.com',
        pass: 'xxwczntgsysriofv'
        //pass: 'Xolopet1010'
    }
});
//    firebase deploy --only "functions:sendEmailConfirmation"
// exports.sendWelcomeEmail = functions.auth.user().onCreate((user) => {
exports.sendEmailConfirmation = functions.database.ref('/pedidos/{uid}').onWrite(async (change) => {

    var men = '<p>Necesita un pedido para su mascota <b>';


    if (change.after._data.mascota != " " && change.after._data.raza != " ") {
        men = '<p>Necesita un pedido para su mascota <b>' + change.after._data.mascota + '</b>, de raza "<b>' + change.after._data.raza + '"</b>.<br/></p>';
    }
    
    const mailOptions = {
        from: '"XolopetApp" <noreply@firebase.com>',
        // to: '<dqniort@gmail.com >, <xolopetapp@gmail.com>',
        to: "xolopetapp@gmail.com",
        subject: 'Solicitud de Pedido XolopetApp',
        //    text: 'Uno de los contenedores de Sebastian sandoval para ramona que esun labrador a la direccion 1 se esta quedando vacío, solicita un pedido. \n Puedes ponerte en contacto con el.',
        html: '<p>Uno de los contenedores modelo: <b>' + change.after._data.model +
         '</b> de <b> ' + change.after._data.nombreu + '</b> se esta quedando vacío.</p>' 
         + men + '<p>Desea Pedir:'+ change.after._data.pedido +'</p><br/><ul>Dirección: <b>' + change.after._data.direccion + '</b></ul>' + '<ul>Telefono de Contacto: <b>' + change.after._data.telefono + '</b> </ul>' + '<ul>Correo Electronico: <b>' + change.after._data.correo + '</b></ul>'
    };

    mailTransport.verify(function (error, success) {
        if (error) {
            console.log("ERROR UNO: " + error);
        } else {
            console.log('MAIL ENVIADO');
        }
    });

    try {
        await mailTransport.sendMail(mailOptions);
    } catch (error) {
        console.log("ESTE ES UN ERRORS 2:");
    }
    return null;

});

//    firebase deploy --only "functions:sendDeviceNotify"
exports.sendDeviceNotify = functions.database.ref('/devices/{devicesUid}').onUpdate(async (snap, context) => {
    var data = {
        'distance': snap.after._data.distance.toString(),
        'mascota': snap.after._data.mascota,
        'model': snap.after._data.model,
        'push': snap.after._data.push.toString(),
        'serial': snap.after._data.serial,
        'user': snap.after._data.user
    };
    var _pet = snap.after._data.mascota;

    if (snap.after._data.push == true) {
        const getDeviceTokensPromise = admin.database().ref(`/users/${
            snap.after._data.user
        }/tokens`).once('value');
        if (snap.after._data.model == "S" && snap.after._data.distance >= 235) {
            if (snap.after._data.mascota.length <= 1) {
                const payload = {
                    data: data,
                    notification: {
                        title: 'Vuelve Amigo',
                        body: 'Uno de tus Dispositivos se está quedando vacío',
                        sound: 'default',
                        clickAction: 'FLUTTER_NOTIFICATION_CLICK',
                        content_available: true
                    }
                };

                const response = await admin.messaging().sendToDevice((await getDeviceTokensPromise).val(), payload).catch(e => {
                    console.log("ERROR: " + e);
                });


            } else {
                const payload = {
                    data: data,
                    notification: {
                        title: 'Vuelve Amigo',
                        body: 'El Dispositivos de ' + _pet + ' se está quedando vacío',
                        sound: 'default',
                        click_action: 'FLUTTER_NOTIFICATION_CLICK',
                        content_available: true
                    }
                };

                const response = await admin.messaging().sendToDevice((await getDeviceTokensPromise).val(), payload).catch(e => {
                    console.log("ERROR: " + e);
                });
            }
        }
        if (snap.after._data.model == "L" && snap.after._data.distance >= 410) {

            if (snap.after._data.mascota.length <= 1) {
                const payload = {
                    data: data,
                    notification: {
                        title: 'Vuelve Amigo',
                        body: 'Uno de tus Dispositivos se está quedando vacío',
                        sound: 'default',
                        click_action: 'FLUTTER_NOTIFICATION_CLICK',
                        content_available: true
                    }
                };

                const response = await admin.messaging().sendToDevice((await getDeviceTokensPromise).val(), payload).catch(e => {
                    console.log("ERROR: " + e);
                });
            } else {
                const payload = {
                    data: data,
                    notification: {
                        title: 'Vuelve Amigo',
                        body: 'El Dispositivos de ' + _pet + ' se está quedando vacío',
                        sound: 'default',
                        click_action: 'FLUTTER_NOTIFICATION_CLICK',  
                        content_available: true
                    }
                };

                const response = await admin.messaging().sendToDevice((await getDeviceTokensPromise).val(), payload).catch(e => {
                    console.log("ERROR: " + e);
                });
            }
        }
    }
});
