'use strict';
const functions = require('firebase-functions');
const nodemailer = require('nodemailer');
const admin = require('firebase-admin');

admin.initializeApp();
const mailTransport = nodemailer.createTransport({
    host: 'smtp.gmail.com', port: 465, secure: true, // service: 'Gmail',
    auth: {
        user: 'xolopetapp@gmail.com',
        pass: 'xxwczntgsysriofv' // pass: 'Xolopet1010
    }
});











// firebase deploy --only "functions:fileUploadedBannerEliminar"
exports.fileUploadedBannerEliminar = functions.storage.object().onDelete(async (object) => {
    if (!object.id.includes("/banner/")) {
        return functions.logger.log('This is not an image for Banners');
    }
    var keyBanner;
    // BUSCAMOS EL KEY DEL DATABASE    
    const valuePromiseToken = await admin.database().ref(`banner`).orderByChild('title').equalTo(object.name).once('value');
    valuePromiseToken.forEach((snapshot) => {
        keyBanner = snapshot.key;
        return
    });
    console.log(keyBanner);
    await admin.database().ref(`/banner/`).child(keyBanner).remove();
    console.log("LISTO!");


    // await admin.database().ref(`banner`).orderByChild('title')
    // .equalTo(object.name)
    // .once('value')
    // .then(function (snapshot) {
    //     console.log("::::::::::::::::::::" );
    // //   console.log(  snapshot.val() );
    //   console.log(  snapshot.key );

    //   console.log("KEYYY:::::" );
    // })
    // console.log(userData);
    // console.log(userData.key );
    // console.log("KEYYY:::::" );
    // CON UN KEY, LO ELIMINAMOS
    // await admin.database().ref(`/banner`).child(userData.key).remove();

    // await admin.database().ref(`/banner`).push().set({             
    //     'url': url,
    //     'idImage':object.id,
    //     'handle': "insert-handle-collection-here",
    //     'title': object.name,
    //     'status': true,
    // });
});
// firebase deploy --only "functions:fileUploadedBannerVer"
exports.fileUploadedBannerVer = functions.storage.object().onFinalize(async (object) => {
    if (!object.id.includes("/banner/")) {
        return functions.logger.log('This is not an image for Banners');
    }
    var url = mediaLinkToDownloadableUrl(object);
    await admin.database().ref(`/banner`).push().set({
        'url': url,
        'idImage': object.id,
        'handle': "insert-handle-collection-here",
        'title': object.name,
        'status': true,
    });
});

// firebase deploy --only "functions:mediaLinkToDownloadableUrl"
function mediaLinkToDownloadableUrl(object) {
    var firstPartUrl = object.mediaLink.split("?")[0] // 'https://www.googleapis.com/download/storage/v1/b/abcbucket.appspot.com/o/songs%2Fsong1.mp3.mp3'
    var secondPartUrl = object.mediaLink.split("?")[1] // 'generation=123445678912345&alt=media'

    firstPartUrl = firstPartUrl.replace("https://www.googleapis.com/download/storage", "https://firebasestorage.googleapis.com")
    firstPartUrl = firstPartUrl.replace("v1", "v0")

    firstPartUrl += "?" + secondPartUrl.split("&")[1]; // 'alt=media'
    firstPartUrl += "&token=" + object.metadata.firebaseStorageDownloadTokens

    return firstPartUrl
}


// // firebase deploy --only "functions:sendPushNotifications"
async function sendPushNotifications(info, tokens, titulo, cuerpo) {
    await admin.messaging().sendMulticast({
        tokens: tokens,
        notification: {
            title: "Xolopet",
            body: "Estamos trabajando en nuestras notificaciones.",
            imageUrl: "https://gitlab.com/dqniort/filesxolopet/-/raw/main/1024px.png",
        },
    });

}


exports.sendEmailPreOrden = functions.database.ref('/preorden/{uid}').onWrite(async (change) => {
    const mailOptions = {
        from: '"XolopetApp" <noreply@firebase.com>',
        to: "xolopetapp@gmail.com",
        subject: 'Nueva PreOrden de Contenedor XolopetApp',
        html: "<h2>Hola, un nuevo usuario solicita un Contenedor.</h2> <h3>Estos son sus datos.</h3> <ul>" + "<li>Nombre: " + change.after._data.nombre + " </li>" + "<li>Correo:  " + change.after._data.correo + " </li>" + "<li>Teléfono: " + change.after._data.telefono + " </li>" + "<li>Raza: " + change.after._data.raza + " </li> " + "<li>Edad:  " + change.after._data.edad + " </li> " + "<li>Marca de Croqueta:  " + change.after._data.marca + " </li>" + "<li>Presentación:  " + change.after._data.presentacion + "</li> </ul>"
    };

    mailTransport.verify(function (error, success) {
        if (error) {
            console.log("ERROR UNO: " + error);
        } else {
            console.log('MAIL ENVIADO');
        }
    });

    try {
        await mailTransport.sendMail(mailOptions);
    } catch (error) {
        console.log("ERRORS 2:" + error);
    }
    return null;
});

function getEmail(data) {
    if (data == undefined)
        return ''
    else
        return data;
}


//firebase deploy --only "functions:sendEmailComentario"
exports.sendEmailComentario = functions.database.ref('/comentarios/{uid}').onWrite(async (snap) => {
    var coment = "";
    if (snap.after._data.generales.length >= 2) {
        coment = "<h3>Comentarios generales:</h3><h2>" + snap.after._data.generales + "</h2>";
    }
    const mailOptions = {
        from: '"XolopetApp" <noreply@firebase.com>',
        to: "xolopetapp@gmail.com",
        subject: 'Nuevo comentario de nuestros usuarios XolopetApp',
        html: "<h2>Hola</h2><br> <p> Un nuevo usuario " + getEmail(snap.after._data.useremail)
            //  + snap.after._data.useremail == undefined  ? 'Invitado' : snap.after._data.useremail 
            + " nos brinda estos comentarios.</p>" +
            "<h3>¿Qué debemos empezar a hacer?</h3> <h2>" + snap.after._data.empezar +
            "</h2><h3>¿Qué debemos seguir haciendo?</h3><h2>" + snap.after._data.mantener +
            "</h2><h3>¿Qué debemos dejar de hacer?</h3><h2>" + snap.after._data.dejar +
            "</h2><h3>¿Encontraste algún error en la App?</h3><h2>" + snap.after._data.error + "<h2>" + coment
    };
    // sendMail(mailOptions);

    mailTransport.verify(function (error, success) {
        if (error) console.log("ERROR UNO: " + error);
    });

    try {
        // await mailTransport.MailMessage(mailOptions); // emit sendMail(mailOptions);
        await mailTransport.sendMail(mailOptions);
    } catch (error) {
        console.log("ERRORS 2:" + error);
    }
    return null;
});

exports.sendEmailPedido = functions.database.ref('/pedidos/{uid}').onWrite(async (change) => {

    var men0 = 'El usuario ' + change.after._data.correo + ' necesita un pedido para su contenedor.'
    var men1 = ' ';
    var men2 = '<p>Desea pedir: <b>' + change.after._data.pedido + '</b></p>';
    var men3 = '</p><br/><ul>Dirección: <b>' + change.after._data.direccion +
        '</b></ul>' + '<ul>Teléfono de Contacto: <b>' +
        change.after._data.telefono + '</b> </ul>' + '<ul>Correo Electronico: <b>'
        + change.after._data.correo + '</b></ul>';

    if (change.after._data.nombreu != undefined) {
        men0 = 'El usuario ' + change.after._data.nombreu + ' necesita un pedido para su contenedor.'
    }
    if (change.after._data.model != undefined) {
        console.log('Si seleccionó un contenedor');
        men0 = men0 + '<br>Serial ' + change.after._data.serial + ' modelo ' + change.after._data.model;
    }
    if (change.after._data.raza != undefined) {
        console.log('Si seleccionó una mascota ');
        men1 = '<p>Su mascota <b>' + change.after._data.nombre
            + '</b> es de Raza </b> ' + change.after._data.raza + '</b></p>';
    }

    const mailOptions = {
        from: '"XolopetApp" <noreply@firebase.com>',
        to: "xolopetapp@gmail.com",
        // to: '<dqniort@gmail.com >',
        subject: 'Solicitud de Pedido XolopetApp',
        html: "<div style='background-color:#e94a47; color: #e94a47; width: 100%; height: 70px;'></div><div><br><br>" +
            men0 + men1 + '</br> ' + men2 + men3 +
            "<br><br></div><div style='background-color:#e94a47; color: #dfd1d1; width: 100%; height: 70px;text-align: center;'></div>"
    };

    mailTransport.verify(function (error, success) {
        if (error) {
            console.log("ERROR UNO: " + error);
        } else {
            console.log('MAIL ENVIADO');
        }
    });

    try {
        await mailTransport.sendMail(mailOptions);
    } catch (error) {
        console.log("ERRORS 2:" + error);
    }
    return null;


    // if (change.after._data.mascota != " " && change.after._data.mascota != "" && change.after._data.mascota  != undefined) {
    // if (change.after._data.mascota  != undefined) {
    //     men1 = '<p>Necesita un pedido para su mascota <b>' + change.after._data.mascota + '</b>.<br/></p>';
    // }

    // if (change.after._data.pedido != " " && change.after._data.pedido != "") {
    //     men2 = '<p>Desea Pedir: <b>' + change.after._data.pedido + '</b></p>';
    // }
    // if (change.after._data.direccion != " ") {
    //     men3 = '</p><br/><ul>Dirección: <b>' + change.after._data.direccion + '</b></ul>' + '<ul>Teléfono de Contacto: <b>' + change.after._data.telefono + '</b> </ul>' + '<ul>Correo Electronico: <b>' + change.after._data.correo + '</b></ul>'
    // }

});

function enviarEmail(optiiones) {
    try {
        // await mailTransport.sendMail(optiiones);
        mailTransport.sendMail(optiiones).then(e => {
            console.log("CORREO ENVIADO (ENVIAR EMAIL FUNCTION) " + error);
        });
    } catch (error) {
        console.log("ERROR ENVIAR EMAIL FUNCTION: ");
        console.log(error);
    }
}

function getBodyPayloadDispo(pet) {
    if (typeof (pet) != "undefined") {
        return 'El Dispositivo de ' + pet + ' se está quedando vacío';
    } else {
        return 'Uno de tus Dispositivos se está quedando vacío';
    }
}


// // firebase deploy --only "functions:getAllTokensByKey"
async function getAllTokensByKey(key) {
    const valuePromiseToken = await admin.database().ref(`/users/${key}/tokens`).once('value');
    let mistokens = [];
    valuePromiseToken.forEach((snapshot) => {
        var key = snapshot.key;
        var valor = snapshot.exportVal();
        mistokens.push(valor);
    });
    return mistokens;
}


// firebase deploy --only "functions:setCantidadToDispo"
exports.setCantidadToDispo = functions.database.ref('/devices/{devicesUid}')
    .onUpdate(async (snap, context) => {
        const snapAnterior = snap.before._data;
        const snapActual = snap.after._data;
        if (snapAnterior.user == undefined && snapActual != null) {
            await admin.database().ref(`/devices/${snap.after.key}`).update({ 'cantidad': snapActual.distance });
        }
    });



// firebase deploy --only "functions:sendDeviceNotifyV2"
exports.sendDeviceNotifyV2 = functions.database.ref('/devices/{devicesUid}')
    .onUpdate(async (snap, context) => {

        const POR_MIN = 25.0;
        //const medidas = { 'S': 290, 'M': 385, 'L': 602, 'XL': 514, };
        const medidas = { 'S': 249, 'M': 366, 'L': 537, 'XL': 467, };
        const data = snap.after._data;

        const porcentaje = (((medidas[data.model] - data.distance) * 100) / (medidas[data.model] - data.cantidad));
        var mybody = getBodyPayloadDispo(data.mascota);

        if (porcentaje > POR_MIN && !data.push) {
            console.log("PRENDEMOS EL DATA PUSH");
            // PRENDEMOS LAS NOTIFIACACIONES
            await admin.database().ref(`/devices/${snap.after.key}`).update({ 'push': true });
        }

        if (porcentaje <= POR_MIN && data.push) {
            const info = {
                "distance": data.distance.toString(),
                "serial": data.serial,
                "push": data.push.toString(),
                "model": data.model,
                "user": data.user,
                "payload": 'level-low',
                "key": snap.after.key,
            };
            if (data.mascota != undefined) info["mascota"] = data.mascota;
            const mistokens = await getAllTokensByKey(data.user);
            // await sendPushNotifications(info, mistokens, 'Vuelve amigo', mybody);    
            console.log(mistokens);
            console.log("MANDAMOS EL PUSH:");
            await admin.messaging().sendMulticast({
                data: info,
                tokens: mistokens,
                notification: {
                    title: 'Vuelve amigo', body: mybody
                },
            });
            console.log("despues de enviart tokens");

            const userData = await admin.database().ref(`/users/${data.user}`).once('value');
            var men = "<h2>Hola, un contenedor se esta quedando vacío..</h2> <ul>"
                + "<li>Contenedor: " + data.serial
                + " </li>" + "<li>Modelo: "
                + data.model + " </li>"
                + "<li>Porcentaje Bateria: "
                + Math.round((data.battery * 100) / 4.5)
                + "% </li>" + "<li>Usuario: "
                + userData.val().nombre + " </li>"
                + "<li>Email: " + userData.val().correo + " </li>";


            // ENVIAR EMAIL
            const mailOptions = {
                from: '"XolopetApp" <noreply@firebase.com>',
                to: "xolopetapp@gmail.com",
                subject: 'Uno de nuestros clientes se esta quedando sin comida.',
                html: men,
            };
            enviarEmail(mailOptions);
        }
        if (data.pushbattery && data.battery <= 0.675) {

            const userData = await admin.database().ref(`/users/${data.user}`).once('value');

            const info = {
                'pushbattery': data.pushbattery.toString(),
                'distance': data.distance.toString(),
                'push': data.push.toString(),
                'serial': data.serial,
                'model': data.model,
                'user': data.user,
                'key': snap.after.key,
                'payload': 'battery-low',
            };

            if (data.mascota != undefined) info["mascota"] = data.mascota;
            if (data.battery != undefined) info["battery"] = data.battery.toString();


            const mistokens = await getAllTokensByKey(data.user);
            // await sendPushNotifications(info, mistokens, 'Vuelve amigo', mybody);            
            await admin.messaging().sendMulticast({
                data: info,
                tokens: mistokens,
                // android: {priority: 'high'},
                notification: {
                    title: 'Vuelve amigo',
                    body: 'Uno de tus Dispositivo se está quedando sin batería.',
                },
            });
            // sendPushNotifications(info, await getAllTokensByKey(data.user), 'Vuelve amigo',
            //     'Uno de tus Dispositivo se está quedando sin batería.');
            //APAGAMOS  LA NOTIFICACION DE BATERÍA
            console.log(data.key);
            await admin.database().ref(`/devices/${data.key}`).update({ 'pushbattery': false });
            //EVNIAMOS UN CORREO A XOLOPET
            var men = "<h2>Hola, un contenedor se esta quedando sin batería.</h2> <ul>"
                + "<li>Contenedor: " + data.serial
                + " </li>" + "<li>Modelo: "
                + data.model + " </li>"
                + "<li>Porcentaje Bateria: "
                + Math.round((data.battery * 100) / 4.5)
                + "% </li>" + "<li>Usuario: "
                + userData.val().nombre + " </li>"
                + "<li>Email: " + userData.val().correo + " </li>";
            const mailOptions = {
                from: '"XolopetApp" <noreply@firebase.com>',
                to: "xolopetapp@gmail.com",
                subject: 'Un contenedor tiene poca batería.',
                html: men
            };
            enviarEmail(mailOptions);
        }
    });





